# WITSML Monitor #

### What is this repository for? ###

* Bootstrap WITSML monitoring setup using docker-compose

### How do I get set up? ###

* Clone the repo
* `docker-compose up -d`

### Keep local config files changes ###
* `git update-index --skip-worktree config.d/prometheus/prometheus.yml`
* `git update-index --skip-worktree config.d/grafana/grafana.env`
* `git update-index --skip-worktree config.d/grafana/provisioning/notifiers/notification.yml`


### Prometheus reload config ###

* modify scrape_configs in `config.d/prometheus/prometheus.yml` 
* reload config by 
  `curl -X POST http://localhost:9090/-/reload` or
  `docker exec prometheus killall -HUP prometheus`
